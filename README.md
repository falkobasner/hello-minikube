# Hello Minikube Project


## Building the project

### Build and package the Java project

    $ ./mvnw package
    
       
## Deployment 

### Minikube setup

#### Enable Minikube ingress addon

    $ minikube addons list 
    
    $ minikube addons enable ingress
    
A pod handling the ingress should be started.
    
    $ kubectl get po --all-namespaces

#### Apply Minikube env

    $ eval $(minikube docker-env)
    
#### Starting Docker registry

    $ docker run -d -p 5000:5000 --restart=always --name registry registry:2


### Build and deploy app

#### Build app Docker image

    $ ./docker-build.sh
    
#### K8s

    $ cd k8s
    
#### Deployment

    $ ./deployment-create.sh
    
#### Service

    $ ./service-create.sh
    
#### Ingress

    $ ./ingress-create.sh            
    

## K8s info

```
minikube ip

kubectl get pods

kubectl get deployments

kubectl get rs

kubectl get services

kubectl get ingresses
```

### Try out cluster ip from inside a running pod (if curl is installed :-))

```
kubectl get pods

kubectl exec <pod name> -- curl -s http://<cluster ip>:<service port>
```