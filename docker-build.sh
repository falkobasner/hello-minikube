#!/bin/bash

# ###############################################################
# init script environment
# ###############################################################
pwd=`pwd`
relDir=`dirname $0`
cd $relDir
THIS_PATH=`pwd`
cd $pwd

echo
# ###############################################################

docker build -t my-hello-minikube -f ./Dockerfile .
