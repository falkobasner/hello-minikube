package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://monster.inc/hello")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Info {

  private String message;
}
