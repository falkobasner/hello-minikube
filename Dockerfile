FROM openjdk:8-jre-alpine

ADD target/app.jar /app.jar

CMD [ \
    "java", \
    "-jar", \
    "/app.jar" \
]
